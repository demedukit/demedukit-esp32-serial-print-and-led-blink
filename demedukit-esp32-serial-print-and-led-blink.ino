#define LED1 32
#define LED2 33

bool ledState = false;
uint32_t last_time = 0;

void setup() {
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, 26, 25);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
}

void loop() {
  if (Serial2.available()) {
    Serial.write(Serial2.read());
  }

  uint32_t currentTime = millis();

  if (currentTime - last_time > 1000) {
    ledState = !ledState;
    digitalWrite(LED1, ledState ? HIGH : LOW);
    digitalWrite(LED2, ledState ? LOW : HIGH);
    last_time = currentTime;
  }
}
